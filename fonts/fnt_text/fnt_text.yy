{
    "id": "562ae245-1b32-4c4f-9e5f-599ed4552bb3",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Bright",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bfe523af-02c4-4d9e-b2a2-6423cfad8c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "210ddaf9-ca26-4b0f-a1c2-3286fbe7c468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 228,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e071bab0-8672-4081-b13b-aba6f51f9e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 220,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "68cd934b-304c-4a5f-a568-dd32d2aba97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 207,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "639c28d3-b1bd-4aae-8f73-1413ddf6a4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 197,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7a671ddf-9635-4f61-a4b2-344210ab3253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 184,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "574f3810-8f4c-4ed4-87ae-2bc3f83192dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 170,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "26f5156e-69b6-4055-8d72-112863875647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a93b7c75-f710-413d-bd97-8ae793cc3ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 158,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d02ec0a4-8990-4353-9104-bc67381e40b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 151,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c6c2be18-55da-470c-9106-54b1bf9556b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 232,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "971b499f-6ece-46da-8247-6c1a0494a2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5fd0f8e5-6fe6-4a2b-914b-d809f00fbb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 124,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "aa64f7e8-f24a-4e97-8898-59d310bdf59d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 117,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "45a1408a-740d-4b9b-8dbd-d3b0a58fa40e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 112,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6ec50e30-90ba-45b2-a999-6592551a7056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 101,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d8eb1b26-4418-4d53-97e1-9dec849eef7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "97b434fc-7b86-42eb-9139-48dacf1d9d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 81,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3c81c3ce-29cd-4bdf-90ff-e28ed50a8f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 71,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7365c937-2578-4fa3-93bd-f6cbe9883a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 61,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e3428325-223b-4fad-80bf-f74ddb686b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "90748191-2743-424d-a192-3f87ce1f58d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ea0112e2-ef07-4194-812e-a40104fd8b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 241,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a2c02a97-a4f6-4391-af54-f7775d6b6b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "565d158c-a75f-4610-bba1-648a4fc7a759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a4e50851-260f-4e6f-8845-2ab5eca46b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3650913e-6484-4339-b83d-84bc2a57548e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 250,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "48b90c1f-1dac-4fbd-8ca3-0c94b9a93634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 245,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b3bd1229-c887-40ae-a064-8fb403e7f447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 233,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "63188289-2b27-4d7e-8ee2-2bf306f5a5e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 221,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "00289291-7abc-41b3-adc8-e2ed8e620d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 209,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "57796621-9812-44ba-bb5a-896c37d4fc70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 200,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d2b58fc2-e296-40ed-a94b-618530b9ae19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "88b74d0e-850d-42ab-8492-fd0013a77e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 170,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "04beb70b-0bce-4596-bb95-e475d220abc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 159,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fb5bbc94-da25-44a8-bdcb-3241fccaa7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "34ad4f80-4937-4f24-8f43-6a117afdd128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 132,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7268b20b-1236-433a-8b47-0def9e1343ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 121,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "743ac9a4-d741-468e-906b-a61d15f06129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d007a9be-615c-49b7-b484-81df6c34e480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 96,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1377463e-dcc7-4639-938d-d517503ece68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "84fb18c2-8155-44dc-93fa-00a981ed6b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5e1796b2-1d54-4b02-a348-2fef4f942926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "75128029-9070-41c8-a21b-5de1eae31c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "795dfcdb-28be-451f-8aeb-d60c213338b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "aa99f797-509b-4858-95ad-9d1cf5a22f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 24,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2b5b7a86-36da-4e23-be3b-cb5cf6558a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 36,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8e1ba3f3-bdb5-4359-83f6-217a1c14abae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a1b598ca-8ee8-4afd-8635-ceef478d24bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e17955bf-f027-45d1-bafa-9eddab023097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 14,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "60ff13e8-bec5-4a3b-a9a6-136e5fae9f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "84ded349-3dcf-4c1e-9885-5d94de220eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "527495f5-b08b-4e3b-9c37-3db581e4284d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "13fc0b07-2d0c-4cd2-addf-737d04fd8c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b610f9e2-ef14-4ea0-81c0-ea4e027c00f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1a9cb453-e534-4c12-9ad3-0b25acb91e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e07e0a02-ad66-4d6e-8a18-95af6314f058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4c3af742-a8df-4eb9-8e5e-424747e9dae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f733fe28-32ca-477b-a5fb-3958a4a8b847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4e88419d-2358-41fb-a035-84fddce571dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f2b8d40d-1985-43d5-99df-8b2b255b781f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e8ecd982-d04c-44ee-99f2-71c69484940d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "676e1611-cfdf-482d-9e19-b1b42064fb27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "607ae76c-0015-4280-b6ec-520155f2a3b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e2fae958-0629-496d-b259-5a7b28e5825e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e8174024-742c-477d-9b16-df29e7aab129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a53e0939-0c63-4c41-9f27-85c2bf08ebab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ba678f09-0a58-4d03-bfda-1e8134ba7eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e8dfd575-b3c2-4590-af5e-19a13c28b380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "479f76ea-498d-4588-a48d-47d0e2907e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a9c3f80d-1320-47d4-908b-0d82d872ea10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4f83ee6b-b4c3-4339-9e15-547230b0f138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8e862557-a8ab-49c2-939e-38fe7d57ec9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a5e6c7a7-9254-479f-b9fd-2e5ca77e0723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 29,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "95c93893-814f-4780-abc6-2d26ebcfbd71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 241,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c2061240-2438-40da-950c-de08a8b5de42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 229,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d45971fc-791a-4102-8595-b4760fd44abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7d5e920e-c8e0-449c-8599-c44dc4e30336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 204,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "256f4382-34b2-430d-af49-eac7bb3a1de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 191,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "46b6e842-d5b8-4b08-804a-5ae0f5fb7d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 180,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7ae3a585-e643-4916-a570-664faa6b0865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 168,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e4845f53-b71a-4ef0-a2c8-094ba2d5c5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2a0439d8-1d92-4d66-9c88-b6b42eddbcc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "96ff8166-6603-4842-803a-e0b823fbecc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3f31167a-b9f7-4eaa-ab1f-96b65d62e4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "36fbe3e9-f8e0-404d-b03f-2b514e1be5b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "75cecc60-4d84-45f9-8f12-f105c331c9cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 103,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e885e048-10a0-4b57-b18a-64aba153ebe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 88,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ba77b032-58f8-4553-8b88-6e7a9aa915b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 76,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f93bdec8-7779-4f33-a64a-a57535b147c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "269805c6-7a0e-423d-95eb-12d21114cfa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 55,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1d757fc2-5ebe-4191-b73d-eb24532f2e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3f23e330-4a94-4973-826b-a6c74f106827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8df0286-d603-4f38-967e-9880adf7c35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 36,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b4609074-7bcb-4830-bac4-6bc53d2cf1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 13,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1c30e188-5075-4afc-80a0-f38af16780c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 25,
                "y": 86
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}