{
    "id": "9db423de-1e2d-4070-80c3-b63d3aed7da0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 3,
    "bbox_right": 59,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bec10b0f-ffac-4a82-8628-63b718499215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db423de-1e2d-4070-80c3-b63d3aed7da0",
            "compositeImage": {
                "id": "5ca9c52c-e681-415e-9fc1-3be93a6e8a0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bec10b0f-ffac-4a82-8628-63b718499215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73a0229-6de9-4fe1-bf57-7eab0cabc7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bec10b0f-ffac-4a82-8628-63b718499215",
                    "LayerId": "42a379e8-c2e4-4218-884e-dfff8281f0f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "42a379e8-c2e4-4218-884e-dfff8281f0f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9db423de-1e2d-4070-80c3-b63d3aed7da0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}