{
    "id": "578156e2-e3f5-49fb-a80e-49b72f894977",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c18871f8-beda-43c1-a7a3-9c2f48b92e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "578156e2-e3f5-49fb-a80e-49b72f894977",
            "compositeImage": {
                "id": "dcb52253-85a8-4dea-96db-825a64f9bc86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c18871f8-beda-43c1-a7a3-9c2f48b92e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31a99fa4-d6e4-4b38-8a4c-6f2cbb3edf22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c18871f8-beda-43c1-a7a3-9c2f48b92e24",
                    "LayerId": "074171e8-cc8c-4c28-b41e-bb7bd304d558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "074171e8-cc8c-4c28-b41e-bb7bd304d558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "578156e2-e3f5-49fb-a80e-49b72f894977",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}