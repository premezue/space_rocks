{
    "id": "5ceaa8a5-b6d0-4341-b5a5-599758e92538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ae7777a-8004-4a06-8e7c-b48245b0dca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ceaa8a5-b6d0-4341-b5a5-599758e92538",
            "compositeImage": {
                "id": "3698e093-ed79-4868-99f2-fea39543d61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ae7777a-8004-4a06-8e7c-b48245b0dca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf622d53-699d-411b-8c3e-f860489ec909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ae7777a-8004-4a06-8e7c-b48245b0dca3",
                    "LayerId": "735ee78f-78be-438d-af0e-25dc65650787"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "735ee78f-78be-438d-af0e-25dc65650787",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ceaa8a5-b6d0-4341-b5a5-599758e92538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}