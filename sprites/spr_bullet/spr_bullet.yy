{
    "id": "85261bc3-fc32-431a-8fd6-0c488e217ba6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d4971c8-5a62-4fa2-8ea4-581f50a07a31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85261bc3-fc32-431a-8fd6-0c488e217ba6",
            "compositeImage": {
                "id": "42de7502-3096-46a4-96cc-e027c8ecbdca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4971c8-5a62-4fa2-8ea4-581f50a07a31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bb6901-e40d-4bba-80db-14950920e60c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4971c8-5a62-4fa2-8ea4-581f50a07a31",
                    "LayerId": "6d84e578-d498-4b34-82c0-522d09e11770"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "6d84e578-d498-4b34-82c0-522d09e11770",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85261bc3-fc32-431a-8fd6-0c488e217ba6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}