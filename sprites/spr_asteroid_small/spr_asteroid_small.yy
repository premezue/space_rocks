{
    "id": "34953539-5c8e-47c6-a6dd-0cbd2f1d11b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5eecc29-017d-4257-8c63-e1566ed5f306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34953539-5c8e-47c6-a6dd-0cbd2f1d11b2",
            "compositeImage": {
                "id": "adf14486-0232-47ad-8d55-930c1652cf07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5eecc29-017d-4257-8c63-e1566ed5f306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1efd84-149b-41b1-bdd0-de809e04f724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5eecc29-017d-4257-8c63-e1566ed5f306",
                    "LayerId": "e772a736-a623-4b74-8a35-1b582b3dd683"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e772a736-a623-4b74-8a35-1b582b3dd683",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34953539-5c8e-47c6-a6dd-0cbd2f1d11b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}