{
    "id": "4145ecec-0bfa-45d7-b1ed-dc624604c705",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": -1,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "690c6114-4b52-478f-b1fc-48c65901356d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4145ecec-0bfa-45d7-b1ed-dc624604c705",
            "compositeImage": {
                "id": "cdb90f48-8d95-4087-b22b-f1af7bf58450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "690c6114-4b52-478f-b1fc-48c65901356d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1653a1-e82a-4ef1-a2db-390c6d9b15bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "690c6114-4b52-478f-b1fc-48c65901356d",
                    "LayerId": "b671179b-3704-4288-a24f-8f23dc15d9f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b671179b-3704-4288-a24f-8f23dc15d9f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4145ecec-0bfa-45d7-b1ed-dc624604c705",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}