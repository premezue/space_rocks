{
    "id": "b4cdc847-87e3-49be-a883-f20efeee02b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "a3b0707b-4fd4-4528-a7f6-9fe574632c97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b4cdc847-87e3-49be-a883-f20efeee02b4"
        },
        {
            "id": "ea6c487f-d41f-4619-8a95-b9ff434d5588",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b4cdc847-87e3-49be-a883-f20efeee02b4"
        },
        {
            "id": "17d7e050-a660-4601-b604-aba9f83b79ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b4cdc847-87e3-49be-a883-f20efeee02b4"
        },
        {
            "id": "ad4bf169-13d0-41d8-aa9c-58d07c4bf305",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b4cdc847-87e3-49be-a883-f20efeee02b4"
        },
        {
            "id": "01e62d8d-b737-4ce8-8e2d-4a644a1cd5a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b4cdc847-87e3-49be-a883-f20efeee02b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}