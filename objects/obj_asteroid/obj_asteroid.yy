{
    "id": "299d994f-6288-4827-a569-29edf29d4937",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "66e8b1e8-fb64-492a-8699-f8e0d23f33df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "299d994f-6288-4827-a569-29edf29d4937"
        },
        {
            "id": "086be202-f553-45ba-8afb-cce4156cc658",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "299d994f-6288-4827-a569-29edf29d4937"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ceaa8a5-b6d0-4341-b5a5-599758e92538",
    "visible": true
}