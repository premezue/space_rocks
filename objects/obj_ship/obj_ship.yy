{
    "id": "d64fbd60-a9e9-429c-95ca-b0997cae4554",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "7097eb20-c728-41b8-8994-0ee33b0f9878",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d64fbd60-a9e9-429c-95ca-b0997cae4554"
        },
        {
            "id": "7c8d4eda-cce9-41a9-bbfa-c0a6f5d1b827",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "299d994f-6288-4827-a569-29edf29d4937",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d64fbd60-a9e9-429c-95ca-b0997cae4554"
        },
        {
            "id": "5f43c7d8-a75e-407f-a9a2-916d0dd966c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d64fbd60-a9e9-429c-95ca-b0997cae4554"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4145ecec-0bfa-45d7-b1ed-dc624604c705",
    "visible": true
}