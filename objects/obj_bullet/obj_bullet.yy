{
    "id": "f22d12e1-713c-492a-9418-5d7285d4d899",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "53e4cc4a-9038-4d65-96c3-66b5ce67c243",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f22d12e1-713c-492a-9418-5d7285d4d899"
        },
        {
            "id": "4781c2bc-d458-47a8-bc82-9f049f372455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "299d994f-6288-4827-a569-29edf29d4937",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f22d12e1-713c-492a-9418-5d7285d4d899"
        },
        {
            "id": "27008dbc-d70d-40fc-89fc-8349a2f00aa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "f22d12e1-713c-492a-9418-5d7285d4d899"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85261bc3-fc32-431a-8fd6-0c488e217ba6",
    "visible": true
}